package ru.tsc.avramenko.tm.service;

import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project startByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project finishByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByName(name, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
